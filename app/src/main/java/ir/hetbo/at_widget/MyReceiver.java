package ir.hetbo.at_widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Created by Hetbo on 8/2/2017.
 */

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("ir.hetbo.at_widget.update_widget")){
            String s = intent.getStringExtra("update");
            updateWidget(context,s);
        }
    }

    private void updateWidget(Context context,String s){
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget);
        remoteViews.setTextViewText(R.id.update, s);
        AWP.pushWidgetUpdate(context.getApplicationContext(), remoteViews);
    }
}