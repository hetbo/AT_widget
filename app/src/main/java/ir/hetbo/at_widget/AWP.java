package ir.hetbo.at_widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Created by Hetbo on 8/2/2017.
 */

public class AWP extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Toast.makeText(context,"ID:"+appWidgetIds.length,Toast.LENGTH_LONG).show();

        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    private void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://hetbo.ir"));
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        views.setTextViewText(R.id.update,"UPDATED");
        views.setOnClickPendingIntent(R.id.web, pendingIntent);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/hetboteam"));
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, i, 0);
        views.setOnClickPendingIntent(R.id.tel, pIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        ComponentName myWidget = new ComponentName(context, AWP.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        manager.updateAppWidget(myWidget, remoteViews);
        Toast.makeText(context,"updated",Toast.LENGTH_SHORT).show();
    }
}
